<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package digicrew
 */

get_header();
?>
<?php digicrew_page_title_layout();?>
	<section class="same-section-spacing single-blog" id="content">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-8 col-md-12">
	                <?php if ( have_posts() ) :
						/* Start the Loop */
						while ( have_posts() ) : the_post(); 
							get_template_part( 'template-parts/content',''); 
						endwhile ; 
					else:
						get_template_part( 'template-parts/content', 'none' );
					endif ; ?>
					<!-- Pagination -->
	        		<?php 		
	        		$next_icon = '<i class="fas fa-arrow-right" aria-hidden="true"></i>';
	                $prev_icon = '<i class="fas fa-arrow-left" aria-hidden="true"></i>';
                    
                    the_posts_pagination(
                        array(
                            'mid_size'  => 2,
                            'prev_text' => $prev_icon,
                            'next_text' => $next_icon,
                        )
                    );
                    ?>
	            </div>
	            
	            <!-- Side-bar -->
	            <div class="col-lg-4 col-md-12">
	                <?php get_sidebar(); ?> 
	            </div>
		        
	        </div>
	        
	    </div>
	</section>
<?php  get_footer(); ?>