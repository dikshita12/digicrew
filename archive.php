<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package digicrew
 */

get_header();
?>
<!-- Banner-saction -->
<section class="banner-bg same-section-spacing">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner-banner-info pt-5">
                    <h2><?php the_archive_title();?></h2>
                </div>
            </div>

        </div>
        <nav aria-label="breadcrumb" class="breadcrumb-right">
            <ol class="breadcrumb">
                <?php if ( function_exists('digicrew_breadcrumbs') ){ 
                    digicrew_breadcrumbs(); 
                }?>
            </ol>
        </nav>
    </div>
</section>
<!-- End-banner-section -->
	<section class="same-section-spacing single-blog" id="content">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-8 col-md-12">
	                <?php if ( have_posts() ) :
	                    /* Start the Loop */
	                    while ( have_posts() ) : the_post(); 
	                        get_template_part( 'template-parts/content' ); 
	                    endwhile ; 
	                else:
	                    get_template_part( 'template-parts/content', 'none' );
	                endif ; ?>
	                <!-- Pagination -->
	        		<?php 		
	        		$next_icon = '<i class="fas fa-arrow-right" aria-hidden="true"></i>';
	                $prev_icon = '<i class="fas fa-arrow-left" aria-hidden="true"></i>';
                    
                    the_posts_pagination(
                        array(
                            'mid_size'  => 2,
                            'prev_text' => $prev_icon,
                            'next_text' => $next_icon,
                        )
                    );
                    ?>
	            </div>
	            <!-- Side-bar -->
                <div class="col-lg-4 col-md-12">
                    <?php get_sidebar(); ?> 
                </div>
	         
	        </div>
	    </div>
	</section>

<?php get_footer(); ?>