<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package digicrew
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'blog-box' ); ?>>
    <?php if( has_post_thumbnail()  ) : ?>
        <div class="wapper-img wapsingleimg">
            <?php the_post_thumbnail('full'); ?>
        </div>
    <?php endif; ?>
    <div class="blog-content">
        <h3><?php the_title(); ?></h3>
        <?php digicrew_post_meta(); ?>
        <div class="blog-data">
            <?php the_content();
            wp_link_pages( 
                array(
                    'before'    => '<nav><ul class="pagination custom-nav">',
                    'after'     => '</ul></nav>',
                    'separator' => ' ',
                ) 
            );
            
           ?>
        </div>
    </div>
</div>
<?php 
    // If comments are open or we have at least one comment, load up the comment template.
    if ( comments_open() || get_comments_number() ) :
        comments_template();
    endif;
?>