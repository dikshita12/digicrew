<!-- Banner-saction -->
<section class="banner-bg same-section-spacing">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner-banner-info pt-5">
                    <h2><?php the_title();?></h2>
                </div>
            </div>

        </div>
        <nav aria-label="breadcrumb" class="breadcrumb-right">
            <ol class="breadcrumb">
                <?php if ( function_exists('digicrew_breadcrumbs') ){ 
                    digicrew_breadcrumbs(); 
                }?>
            </ol>
        </nav>
    </div>
</section>
<!-- End-banner-section -->