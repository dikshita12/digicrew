<!-- Header-start -->
<header class="header-one">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <?php if (has_custom_logo()) { 
                the_custom_logo(); 
            } ?>
            <?php if (display_header_text() == true) { ?>
            <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
            </a>
            <?php } ?>
           
            <div class="collapse navbar-collapse my-lg-0" id="navbarNav">
                <?php 
                wp_nav_menu( array (
                      'container'      => 'ul',
                      'theme_location' => 'primary',
                      'items_wrap'     => '<ul class="navbar-nav">%3$s</ul>',
                    ) 
                );
                ?>
            </div>
        </nav>
        <?php $custom_logo_id = get_theme_mod( 'custom_logo' ); 
        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        if ( has_custom_logo() ) { 
        ?>
            <div class="mobile-menu" data-type="logo" data-link="<?php echo esc_attr( home_url('/') ); ?>" data-logo="<?php echo esc_attr( $logo[0] ); ?>"> </div>
        <?php } else{ ?>
            <div class="mobile-menu" data-type="text" data-link="<?php echo esc_attr( home_url('/') ); ?>" data-logo="<?php bloginfo( 'name' ); ?>"> </div>
        <?php } ?>
    </div>
</header>
<!-- Header-end -->