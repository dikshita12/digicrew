<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package digicrew
 */

get_header();
?>
	<?php digicrew_page_title_layout();?>
	<section class="same-section-spacing single-blog page-box" id="content">
	    <div class="container">
	    	<div class="row">
		    	<div class="col-lg-8 col-md-12">
					<?php
					while ( have_posts() ) : the_post(); ?>
						
							<?php 
							the_content();
							wp_link_pages( 
								array(
									'before'    => '<nav><ul class="pagination custom-nav">',
									'after'     => '</ul></nav>',
									'separator' => ' ',
								) 
							);
							?>
						
						<?php 
						if ( comments_open() || get_comments_number() ) { ?>
							<?php comments_template();?>
						<?php } 
					endwhile; // End of the loop.
					?>	
				</div>
				
		        <div class="col-lg-4 col-md-12">
		            <?php get_sidebar(); ?>  
		        </div>
			   
	    	</div>
	    </div>
	</section>
<?php get_footer(); ?>