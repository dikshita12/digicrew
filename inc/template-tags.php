<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package digicrew
 */

if ( ! function_exists( 'wp_body_open' ) ) :
	/**
	 * Shim for sites older than 5.2.
	 *
	 * @link https://core.trac.wordpress.org/ticket/12563
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
endif;

/**
 * Prints archive meta on blog
 */
if ( ! function_exists( 'digicrew_archive_meta' ) ) :
    function digicrew_archive_meta() {
        ?>
			<ul class="blog-meta">
        		
            	<li><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID')),'digicrew'); ?>"><i class="fas fa-user"></i><?php the_author(); ?></a></li>
            
            	<li> <i class="far fa-calendar-alt"></i><?php echo esc_html(get_the_date()); ?></li>
            	<?php if(has_category()){ ?>
	            <li><i class="fas fa-tag"></i><?php the_category(' , '); ?></li>
	            <?php } ?>
            </ul>
		<?php 
	}
endif;

/**
 * Prints post meta on blog
*/
if ( ! function_exists( 'digicrew_post_meta' ) ) :
    function digicrew_post_meta() { ?>
    	<ul class="blog-meta">
    		
        	<li><a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID')),'digicrew'); ?>"><i class="fas fa-user"></i> <?php the_author(); ?></a></li>
        
        	<li> <i class="far fa-calendar-alt"></i> <?php echo esc_html(get_the_date()); ?></li>
       		<?php if(has_tag()){ ?>
        	<li><i class="fas fa-tag"></i><?php the_tags('',' , '); ?></li>
        	<?php } ?>
        	<?php if(has_category()){ ?>
            <li><i class="fas fa-tag"></i><?php the_category(' , '); ?></li>
            <?php } ?>
        </ul>
    <?php 
	}
endif; ?>