<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package digicrew
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function digicrew_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'digicrew_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function digicrew_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'digicrew_pingback_header' );

/**
 * Header layout
**/
function digicrew_header_layout() {
    get_template_part( 'template-parts/header' );
}
/**
 * Page title layout
 **/
function digicrew_page_title_layout() {
    get_template_part( 'template-parts/entry-header', '' );
}
/**
 * Custom Comment List
*/
function digicrew_comment_list( $comment, $args, $depth ) {
    if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    ?>
    <<?php echo ''.esc_attr($tag); ?> <?php comment_class( empty( $args['has_children'] ) ? 'comment-item ' : 'comment-item parent' ) ?> id="comment-<?php comment_ID() ?>">
    
    <div class="left-comment-box">
        <?php if ($args['avatar_size'] != 0) echo wp_kses_post(get_avatar($comment, $args['avatar_size'])); ?>
    </div>
    <div class="right-comment-content">
        <h3 class="comment-title"><?php printf('%s', get_comment_author_link()); ?> |<span><?php echo esc_html(get_comment_date()) . ' at ' . esc_html(get_comment_time()); ?></span></h3>
        <?php esc_url(comment_reply_link(array_merge($args, array('reply_text' => __('<i class="fas fa-reply-all"></i>', 'digicrew'),'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'])))); ?>
        <?php if ( $comment->comment_approved == '0' ) : ?>
        <p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'digicrew' ); ?></p>
        
        <?php endif; ?> 
        <?php esc_html(comment_text()); ?>

    </div>
    
<?php 
}

/***************** Create the breadcrumbs ******************/

if (!function_exists('digicrew_breadcrumbs')) {
    
    function digicrew_breadcrumbs() {

        $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show

        $home = esc_html__('Home', 'digicrew'); // text for the 'Home' link

        $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show

        global $post;

        $homeLink = home_url();

        if (is_home() || is_front_page()) {
            if ($showOnHome == 1) echo '<li class="breadcrumb-item"><a href="'.esc_url($homeLink).'">'.esc_html($home).'</a></li>';
        }
        else {
            echo '<li class="breadcrumb-item"><a href="'.esc_url($homeLink).'">'.esc_html($home).'</a></li>';
            //echo '<li class="breadcrumb-item active">';

            if ( is_category() ) {
              
                global $wp_query;

                $cat_obj = $wp_query->get_queried_object();

                $thisCat = $cat_obj->term_id;

                $thisCat = get_category($thisCat);

                $parentCat = get_category($thisCat->parent);

                if ($thisCat->parent != 0) echo '<li class="breadcrumb-item">'.wp_kses_post(get_category_parents($parentCat, TRUE, ' ')).'</li>';

                echo '<li class="breadcrumb-item active">'. esc_html__('Archive by category', 'digicrew').' "' . single_cat_title('', false) . '"'."</li>";

            }
            elseif ( is_search() ) {
         
                echo '<li class="breadcrumb-item active">'.esc_html__('Results for', 'digicrew').' "' . get_search_query() . '"'."</li>";
            }
            elseif ( is_day() ) {
              
                echo '<li class="breadcrumb-item"><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y') ). '</a></li> ';

                echo '<li class="breadcrumb-item"><a href="' . esc_url(get_month_link(get_the_time('Y'),get_the_time('m'))) . '">' . esc_html(get_the_time('F')) . '</a></li> ';

                echo '<li class="breadcrumb-item active">'.esc_html(get_the_time('d'))."</li>";
            }
            elseif ( is_month() ) {
             
                echo '<li class="breadcrumb-item"><a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . esc_html(get_the_time('Y')) . '</a></li> ';

                echo '<li class="breadcrumb-item active">'.esc_html(get_the_time('F'))."</li>";

            }
            elseif ( is_year() ) {
            
                echo '<li class="breadcrumb-item active">'.esc_html(get_the_time('Y'))."</li>";

            }
            elseif ( is_single() && !is_attachment() ) {
            
                if ( get_post_type() != 'post' ) {

                    $post_type = get_post_type_object(get_post_type());

                    $slug = $post_type->rewrite;

                    echo '<li class="breadcrumb-item"><a href="' . esc_url($homeLink) . '/' . esc_url($slug['slug']) . '/">' . esc_html($slug['slug']) . '</a></li>';

                    if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.esc_html(get_the_title()).'</li>';
                }
                else {
             
                    $cat = get_the_category(); $cat = $cat[0];

                    is_wp_error( $cats = get_category_parents($cat, TRUE, ' ') ) ? '' : $cats; 

                    if ($showCurrent == 0) $cats = preg_replace("/^(.+)\s \s$/", "$1", $cats);

                    if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.wp_kses_post(get_the_title()).'</li>';
                }
            }
            elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {

                $post_type = get_post_type_object(get_post_type());

                echo esc_html(get_post_type_object(get_post_type()))->labels->singular_name;
            }
            elseif ( is_attachment() ) {
                    
                $parent = get_post($post->post_parent);


                echo '<li class="breadcrumb-item"><a href="' . esc_url(get_permalink($parent)) . '">' . esc_html($parent->post_title) . '</a></li>';

                if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.esc_html(get_the_title()).'</li>';
            }
            elseif ( is_page() && !$post->post_parent ) {

                if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.esc_html(get_the_title()).'</li>';
            }
            elseif ( is_page() && $post->post_parent ) {

                $parent_id  = $post->post_parent;

                $breadcrumbs = array();

                while ($parent_id) {

                    $page = get_page($parent_id);

                    $breadcrumbs[] = '<li class="breadcrumb-item"><a href="' . esc_url(get_permalink($page->ID)) . '">' . esc_html(get_the_title($page->ID)) . '</a></li> ';

                    $parent_id  = $page->post_parent;
                }

                $breadcrumbs = array_reverse($breadcrumbs);

                foreach ($breadcrumbs as $crumb) echo !empty( $crumb ) ? wp_kses_post($crumb) : '';

                if ($showCurrent == 1) echo '<li class="breadcrumb-item active">'.esc_html(get_the_title()).'</li>';

            }
            elseif ( is_tag() ) {

                echo '<li class="breadcrumb-item active">'.esc_html__('Posts tagged for ', 'digicrew').' "' . single_tag_title('', false) . '"'.'</li>';

            }
            elseif ( is_author() ) {

                global $author;

                $userdata = get_userdata($author);

                echo '<li class="breadcrumb-item active">'.esc_html__('Articles posted by', 'digicrew').' ' . esc_html($userdata->display_name).'</li>';

            }
            elseif ( is_404() ) {

                echo '<li class="breadcrumb-item active">'.esc_html__('Error 404', 'digicrew').'</li>';

            }

            

            //echo '</li>';
        }
    }
}

if ( ! function_exists( 'digicrew_customize_search_form' ) ) :

    /** Customize search form.
     **/
    function digicrew_customize_search_form() {

        $form = '<div class="search-box"><form method="post" id="searchform" action="' . esc_url( home_url( '/' ) ) . '">
          <input type="text" class="form-control" placeholder="' . esc_attr_x( 'Enter your keywords...','placeholder', 'digicrew' ) . '" value="' . get_search_query() . '" name="s" />
                <button type="submit" class="fa fa-search btn-search"></button>
            </form></div>';
        return $form;
    }
    
endif;

add_filter( 'get_search_form', 'digicrew_customize_search_form', 15 );