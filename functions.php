<?php
/**
 * digicrew functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package digicrew
 */

if ( ! function_exists( 'digicrew_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function digicrew_setup() {
		/*
	     * Make theme available for translation.
	     * Translations can be filed in the /languages/ directory.
	     * If you're building a theme based on Theme Palace, use a find and replace
	     * to change 'digicrew' to the name of your theme in all the template files.
	     */
	    load_theme_textdomain( 'digicrew' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary', 'digicrew' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.

		$args =	array(
					'default-color' => 'ffffff',
					'default-image' => '',
				);
		
		add_theme_support( 'custom-background', $args);

		/* Gutenberg */
	    add_theme_support('wp-block-styles');
	    add_theme_support('align-wide');
		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/* Add editor style. */
	    add_theme_support('editor-styles');
	    add_theme_support('dark-editor-style');

	     /*
	     * This theme styles the visual editor to resemble the theme style,
	     * specifically font, colors, icons, and column width.
	    */
	    add_editor_style('/assets/css/editor-style.css');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'digicrew_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function digicrew_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'digicrew_content_width', 640 );
}
add_action( 'after_setup_theme', 'digicrew_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function digicrew_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'digicrew' ),
			'id'            => 'sidebar',
			'description'   => esc_html__( 'Add widgets here.', 'digicrew' ),
			'before_widget' => '<div id="%1$s" class="widget sidebar-box %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="title">',
			'after_title'   => '</h3>',
		)
	);
	register_sidebar(
		array(
	        'name'          => esc_html__('Footer Widget Area', 'digicrew'),
	        'id'            => 'footer-widget-area',
	        'description'   => esc_html__('footer widget area', 'digicrew'),
	        'before_widget' => '<div class="col-md-3 col-sm-6"><div id="%1$s" class="%2$s widget footer-box">',
	        'after_widget'  => '</div></div>',
	        'before_title'  => '<h3>',
	        'after_title'   => '</h3>',
	    )
	);
}
add_action( 'widgets_init', 'digicrew_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function digicrew_scripts() {

	wp_enqueue_style( 'google-fonts',          digicrew_fonts_url() ); 
	wp_enqueue_style('bootstrap', get_template_directory_uri() .'/assets/css/bootstrap.css');
	wp_enqueue_style('animate', get_template_directory_uri() .'/assets/css/animate.css');
	wp_enqueue_style('fontawesome', get_template_directory_uri() .'/assets/css/all.css');
	wp_enqueue_style( 'slicknav',   get_template_directory_uri() . '/assets/css/slicknav.css' );
	wp_enqueue_style( 'digicrew-style', get_stylesheet_uri());
	wp_enqueue_style('digicrew-responsive', get_template_directory_uri() .'/assets/css/responsive.css');
	
	wp_enqueue_script( 'bootstrap',        get_template_directory_uri() . '/assets/js/bootstrap.js', array( 'jquery' ), true, true); 
	wp_enqueue_script( 'jquery-slicknav',  get_template_directory_uri() . '/assets/js/jquery.slicknav.js', array( 'jquery' ), true, true); 
    wp_enqueue_script( 'slick', 	       get_template_directory_uri() . '/assets/js/slick.js', array( 'jquery' ), true, true);
	wp_enqueue_script( 'digicrew-custom',  get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), true, true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'digicrew_scripts' );

if ( ! function_exists( 'digicrew_fonts_url' ) ) :
    /**
     * Register Google fonts.
     *
     * Create your own digicrew_fonts_url() function to override in a child theme.
     *
     * @since league 1.1
     *
     * @return string Google fonts URL for the theme.
     */
    function digicrew_fonts_url()
    {
        $fonts_url = '';
        $fonts     = array();
        $subsets   = 'latin,latin-ext';

        if ( 'off' !== _x( 'on', 'Hind Siliguri font: on or off', 'digicrew' ) )
        {
            $fonts[] = 'Hind Siliguri:400,500,600';
        }

        if ( 'off' !== _x( 'on', 'Work Sans font: on or off', 'digicrew' ) )
        {
            $fonts[] = 'Work Sans:400,600,700,800';
        }

        if ( $fonts ) {
            $fonts_url = add_query_arg( array(
                'family' => urlencode( implode( '|', $fonts ) ),
                'subset' => urlencode( $subsets ),
            ), 'https://fonts.googleapis.com/css' );
        }
		return $fonts_url;
    }
endif;

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';