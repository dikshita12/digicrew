<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package digicrew
 */

get_header();
?>	
	<?php digicrew_page_title_layout();?>
	<section class="same-section-spacing single-blog" id="content">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-8 col-md-12">
	            	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	                	<?php get_template_part( 'template-parts/content','single' ); ?>
	                <?php endwhile; endif; ?>
	            </div>
	            <!-- Side-bar -->
                <div class="col-lg-4 col-md-12">
                    <?php get_sidebar(); ?>
                </div>
	        </div>
	    </div>
	</section>
<?php  get_footer(); ?>