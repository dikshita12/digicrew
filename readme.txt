=== digicrew ===

Contributors: weblizar
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready

Requires at least: 4.5
Tested up to: 5.5
Requires PHP: 5.6
Stable tag: 1.0.0
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A starter theme called digicrew.

== Description ==

DIGICREW , A Flexible WordPress theme specially designed for Digital Marketing Agencies and SEO Agencies ,
SEO optimized WordPress theme for Digital Marketing Agency . Great WordPress theme for Online Marketing, SEO & Social Media Agency.


== Frequently Asked Questions ==

= How to Install Theme =

1. In your admin panel, go to Appearance > Themes and click the Add New button and Search for blog starter, OR
2. If you have theme zip file, click Upload and choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Changelog ==

= 1.0.0
* Initial release

digicrew bundles the following third-party resources:

* Based on Underscores https://underscores.me/, (C) 2012-2020 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css https://necolas.github.io/normalize.css/, (C) 2012-2018 Nicolas Gallagher and Jonathan Neal, [MIT](https://opensource.org/licenses/MIT)

= BUNDELED CSS & js = 

    Font Awesome Free 5.8.2 by @fontawesome - https://fontawesome.com
 	License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

 	animate.css -http://daneden.me/animate
    Version - 3.7.0
    Licensed under the MIT license - http://opensource.org/licenses/MIT
    Copyright (c) 2018 Daniel Eden

    Bootstrap v4.3.1 (https://getbootstrap.com/)
 	Copyright 2011-2019 The Bootstrap Authors
	Copyright 2011-2019 Twitter, Inc.
 	Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

 	SlickNav Responsive Mobile Menu v1.0.10
	(c) 2019 Josh Cope
 	licensed under MIT

== License/copyright for images ==

* logo.png self created.