/*--------------------------------------------------
Project:        Digicrew
Version:        1.0
Author:         Company Name
-----------------------------------------------------

    JS INDEX
    ================================================
    * preloader js
    * sticky menu js
    * slick nav 
    * slick icon slider
    * counter
    * testimonial slider
    * slick icon slider
    * bottom to top
    * Isotop with ImagesLoaded
    * ACCORDION WITH TOGGLE ICONS
    * Google-Map
    ================================================*/
    
    (function($) {
      "use strict";
  
      var $main_window = $(window);
  
      /*====================================
      preloader js
    ======================================*/
      $main_window.on('load', function() {
          $('#preloader').fadeOut('slow');
      });
  
      /*====================================
      sticky menu js
    ======================================*/
      var windows = $(window);
      var sticky = $('.header-one')
      windows.on('scroll', function() {
          var scroll = windows.scrollTop();
          if (scroll < 50) {
              sticky.removeClass('stick');
          } else {
              sticky.addClass('stick');
          }
      });
      /*====================================
          slick nav
      ======================================*/
      var logo_path = $('.mobile-menu').data('logo');
      var logo_link = $('.mobile-menu').data('link');
      var logo_type = $('.mobile-menu').data('type');
      let logo;
      if (logo_type === 'text') {
        logo = '<a href="' + logo_link + '"><h1 class="site-title">'+logo_path+'</h1></a>';
      }else {
        logo = '<a href="' + logo_link + '"><img src="' + logo_path + '" class="img-fluid" alt="logo"></a>';;
      }

      $('.navbar-nav').slicknav({
          appendTo: '.mobile-menu',
          removeClasses: true,
          label: '',
  
          closedSymbol: '<i class="fa fa-angle-right"><i/>',
          openedSymbol: '<i class="fa fa-angle-down"><i/>',
          brand: logo
      });
      
      /*====================================
          bottom to top
      ======================================*/
      var btn = $('#btn-to-top');
  
      $(window).scroll(function() {
          if ($(window).scrollTop() > 300) {
              btn.addClass('show');
          } else {
              btn.removeClass('show');
          }
      });
  
      btn.on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({
              scrollTop: 0
          }, '300');
      });

      jQuery(document).ready(function() {
          jQuery(".navbar-nav").accessibleDropDown();
      });

      jQuery.fn.accessibleDropDown = function () {
        var el = jQuery(this);

        /* Make dropdown menus keyboard accessible */

          jQuery("a", el).focus(function() {
              jQuery(this).parents("li").addClass("force-show");
          }).blur(function() {
              jQuery(this).parents("li").removeClass("force-show");
          });
      }
      
      jQuery(document).ready(function() {
          jQuery(".slicknav_nav").accessibleDropDown();
      });

      jQuery.fn.accessibleDropDown = function () {
        var el = jQuery(this);

        /* Make dropdown menus keyboard accessible */

          jQuery("a", el).focus(function() {
              jQuery(this).parents("li").addClass("force-show");
          }).blur(function() {
              jQuery(this).parents("li").removeClass("force-show");
          });
      }
})(jQuery);