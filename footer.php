<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package digicrew
 */

?>

	<!-- Footer-section -->
	<footer class="footer-bg">
	    <?php  if (is_active_sidebar('footer-widget-area')) { ?>
	        <div class="ws-section-spacing">
	            <div class="container">
	                <div class="row">
	                    <?php dynamic_sidebar('footer-widget-area'); ?>
	                </div>
	            </div>
	        </div>
	    <?php } ?>
	    <div class="copyright-area">
	        <div class="container">
	            <div class="row">
	                <div class="col-lg-12 text-center">
	                    <p>
                        	<?php echo esc_html( gmdate( 'Y' ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?> <?php echo esc_html__( 'All rights reserved.', 'digicrew' ); ?>
                        </p>
	                </div>
	            </div>
	        </div>
	    </div>
	</footer>
	<!-- End-footer-section -->
</div><!-- #page -->
<a id="btn-to-top"></a>
<?php wp_footer(); ?>

</body>
</html>